const Router = require('express')
const router = new Router()
const todoController = require('../controller/todo.controller.tsx')

router.post('/todo', todoController.createTodo)
router.post('/todo/get', todoController.getTodo)
router.put('/todo', todoController.updateTodo)
router.delete('/todo', todoController.deleteTodo)

module.exports = router
