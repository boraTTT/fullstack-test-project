# todo-list

For running app use "npm run dev", this script will run client and server.

You also can manually run app.

Use for it "nodemon server.tsx" for running server at 'localhost:5000' and
"cd to-do-list && npm run start" for running client at 'localhost:3000'.

For running tests use "npm test" of "jest".

Don`t forget for configure date base! In project use postgresql.
