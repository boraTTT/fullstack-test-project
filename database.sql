create TABLE user(
  id SERIAL PRIMARY KEY NOT NULL UNIQUE,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL
);

create TABLE todo(
    id SERIAL PRIMARY KEY NOT NULL UNIQUE,
    text VARCHAR(500),
    isConfirmed INTEGER NOT NULL,
    userId INTEGER,
    FOREIGN KEY (user_id) REFERENCES user (id)
);
