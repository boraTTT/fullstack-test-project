import * as React from 'react';
import Registration from './components/Registration';
import Login from './components/Login';
import './styles/App.scss'
import {
  Switch,
  Route,
  Link
} from 'react-router-dom';

const App = () => {
  const logout = () => {
    localStorage.removeItem('authorizationData')
  }
  return (
    <div className={'app'}>
      <nav>
        <span className={'app__logo'}>TO-DO List</span>

        <div className={"app__nav-bar"}>
          <button className={"app__nav-bar-btn"}>Menu</button>
          <div className={"app__nav-bar-content"}>
            <Link
              onClick={logout}
              to={'/registration'}
            >
              Logout
            </Link>
            <Link
              onClick={logout}
              to={'/registration'}
            >
              Sign Up
            </Link>
          </div>
        </div>
      </nav>

      <Switch>
        <Route path={'/login'}>
          <Login/>
        </Route>
        <Route path={'/registration'}>
          <Registration/>
        </Route>
      </Switch>
      <Login/>
    </div>
  )
}

export default App;
