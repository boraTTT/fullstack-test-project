import * as React from 'react';
import { ToDos } from '../store/ToDoList';
import '../styles/Registration.scss'

const Registration: React.FunctionComponent = (): JSX.Element => {
    const [isLogin, setIsLogin] = React.useState(false)
    const [username, setUsername] = React.useState('')
    const [password,setPassword] = React.useState('')
    const [isLoginValid,setIsLoginValid] = React.useState(false)
    const [loginErrorMessage,setLoginErrorMessage] = React.useState('')
    const [passwordErrorMessage,setPasswordErrorMessage] = React.useState('')
    const [isPasswordValid,setIsPasswordValid] = React.useState(false)
    const [isPasswordMatch,setIsPasswordMatch] = React.useState(false)
    const [id, setId] = React.useState(0)
    const onUsernameChange = (e: React.FocusEvent<HTMLInputElement>) => {
        if (e.target.value.length > 7) {
            setIsLoginValid(true)
            setLoginErrorMessage('')
        } else {
            setIsLoginValid(false)
            setLoginErrorMessage('Minimum login length 9 characters')
        }
        setUsername(e.target.value)
    }
    const onPasswordChange = (e: React.FocusEvent<HTMLInputElement>) => {
        if (e.target.value.length > 7) {
            setIsPasswordValid(true)
            setPasswordErrorMessage('')
        } else {
            setIsPasswordValid(false)
            setPasswordErrorMessage('Minimum password length 9 characters')
        }
        setPassword(e.target.value)
    }
    const onConfirmPasswordChange = (e: React.FocusEvent<HTMLInputElement>) => {
        if (e.target.value === password) {
            setIsPasswordMatch(true)
        } else {
            setIsPasswordMatch(false)
        }
    }
    const submit = async () => {
        if (isLoginValid && isPasswordMatch && isPasswordValid) {
            try {
                await
                  fetch('/api/user', {
                      method: 'POST',
                      body: JSON.stringify({ username, password, isLogin}),
                      headers: {
                          'Content-type': 'application/json'
                      }
                  }).then(response => response.json())
                    .then(data => {
                        if (data) {
                            setId(data)
                            setIsLogin(true)
                            localStorage.authorizationData = JSON.stringify({
                                'username': username,
                                'password': password,
                                'userId': data
                            })
                        } else {
                            alert('A user with the same Full name already exists!')
                        }
                    })
            } catch (e) {
                alert('Something is wrong... Try latter!')
            }
        } else {
            alert('Сheck the correctness of the entered data')
        }
    }
    if (isLogin) {
        return <ToDos id={id}/>
    } else {
        return(
          <div className={'registration'}>
              <div className={'registration__box'}>
                  <h1>Sign Up</h1>
                  <div className={'registration__box-user'}>
                      <i className={'registration__box-fas'}></i>
                      <p className={'registration__error-message'}>{loginErrorMessage}</p>
                      <input
                        type={'text'}
                        name={'username'}
                        id={'registrationUsername'}
                        onChange={onUsernameChange}
                        placeholder={'Full name'}
                      />
                      <i className={'registration__box-fas'}></i>
                      <p className={'registration__error-message'}>{passwordErrorMessage}</p>
                      <input
                        type={'password'}
                        name={'password'}
                        id={'registrationPassword'}
                        onChange={onPasswordChange}
                        placeholder={'Password'}
                      />
                      <i className={'registration__box-fas'}></i>
                      <input
                        type={'password'}
                        name={'password'}
                        id={'registrationConfirmPassword'}
                        onChange={onConfirmPasswordChange}
                        placeholder={'Confirm password'}
                      />
                  </div>

                  <div className={'registration__box-login'}>
                      <button
                        className={'registration__box-login-btn'}
                        onClick={submit}
                      >
                          Submit
                      </button>
                      <p className={'registration__box-login-signup'}>
                          Already have an account?
                          <a href={'./signIn'}>Log in</a>
                      </p>
                  </div>
              </div>
          </div>
        )
    }
}

export default Registration;
