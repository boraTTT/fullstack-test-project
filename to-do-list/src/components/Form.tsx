import * as React from 'react';
import '../styles/Form.scss'

interface Props {
  addNewBrick?: any,
  userId: number,
};

const Form: React.FunctionComponent<Props> = (props => {
  const [text, setText] = React.useState('')
  const onInputChange = (e: React.FocusEvent<HTMLInputElement>) => setText(e.target.value);
  const addTodo = () => {
    if (text) {
      props.addNewBrick(text, props.userId)
      setText('')
    }
  }
  return (
    <div className={'form'}>
      <input
        placeholder={'Write to me!'}
        type={'text'}
        value={text}
        name={'text'}
        className={'form__input'}
        onChange={onInputChange}
      />
      <button onClick={addTodo} className={'form__btn'}>
        {'Add to-do'}
      </button>
    </div>
  )
})

export default Form;
