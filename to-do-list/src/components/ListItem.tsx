import * as React from 'react';
import '../styles/ListItem.scss'

interface Props {
  data: {
    id: number,
    text: string,
    isconfirmed: number,
  },
  changeOfStatusTodos: any,
  userid: number,
  removeTodo: any,
};

const ListItem: React.FunctionComponent<Props> = (props => {
    const changeOfStatusTodos = () => {
      let status = props.data.isconfirmed ? 0 : 1
        props.changeOfStatusTodos(status, props.data.id, props.userid)
    }
    const removeTodo = () => {
        props.removeTodo(props.data.id, props.userid)
    }
  return (
      <div className={'list-item'}>
        <button
          className={'list-item__remove-btn'}
          onClick={removeTodo}
        >
        </button>
        <input
            className={'list-item__checkbox'}
            type={'checkbox'}
            onChange={changeOfStatusTodos}
            checked={props.data.isconfirmed === 1}
            id={`checkbox-${props.data.id}`}
            name={`checkbox-${props.data.id}`}
          />
          <label htmlFor={`checkbox-${props.data.id}`}/>
        <span className={'list-item__paragraph'}>{props.data.text}</span>
      </div>)
})

export default ListItem;
