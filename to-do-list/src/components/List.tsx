import * as React from 'react';
import ListItem from './ListItem';

interface Props {
  todos: Array<{
    id: number,
    text: string,
    isconfirmed: number,
  }>,
  changeOfStatusTodos: any,
  removeTodo: any,
  userid: number,
};

const List: React.FunctionComponent<Props> = (props => {
  return (
      <div>{
        props.todos.map(item =>
          <ListItem
              changeOfStatusTodos={props.changeOfStatusTodos}
              removeTodo={props.removeTodo}
              userid={props.userid}
              key={item.id}
              data={item}
          />
        )}
      </div>
  )
})

export default List;
