import * as React from 'react';
import { ToDos } from '../store/ToDoList';
import '../styles/Login.scss'

const Login: React.FunctionComponent = (): JSX.Element => {
    const [isLogin, setIsLogin] = React.useState(true)
    const [username, setUsername] = React.useState('')
    const [password,setPassword] = React.useState('')
    const [id, setId] = React.useState(0)
    React.useEffect(() => {
        if (localStorage.authorizationData) {
            const authorizationData = JSON.parse(localStorage.authorizationData)
            loginRequest(authorizationData.username, authorizationData.password, false)
        }
    }, [])
    const loginRequest = async (loginUsername: string, loginPassword: string,isLoginRequest: boolean) => {
        try {
            await
              fetch('/api/user', {
                  method: 'POST',
                  body: JSON.stringify({ username, password, isLogin }),
                  headers: {
                      'Content-type': 'application/json'
                  }
              }).then(response => response.json())
                .then(data => {
                    if (data) {
                        setId(data)
                        setIsLogin(false)
                        if (isLoginRequest) {
                            localStorage.authorizationData = JSON.stringify({
                                'username': username,
                                'password': password,
                                'userId': data,
                            })
                        }
                    } else if (isLoginRequest) {
                        alert('Wrong username or password!')
                    }
                })
        } catch (e) {
            alert('Something is wrong... Try latter!')
        }
    }
    const onUsernameChange = (e: React.FocusEvent<HTMLInputElement>) => {
        setUsername(e.target.value)
    }
    const onPasswordChange = (e: React.FocusEvent<HTMLInputElement>) => {
        setPassword(e.target.value)
    }
    const submit = () => {
        loginRequest(username, password, true)
    }
    if (isLogin) {
        return(
          <div className={'login'}>
              <div className={'login__box'}>
                  <h1>Log in</h1>
                  <div className={'login__box-user'}>
                      <i className={'login__box-fas'}></i>
                      <input
                        type={'text'}
                        name={'username'}
                        id={'loginUsername'}
                        onChange={onUsernameChange}
                        placeholder={'Full name'}
                      />
                      <i className={'login__box-fas'}></i>
                      <input
                        type={'password'}
                        name={'password'}
                        id={'loginPassword'}
                        onChange={onPasswordChange}
                        placeholder={'Password'}
                      />
                      <i className={'login__box-fas'}></i>
                  </div>

                  <div className={'login__box-login'}>
                      <button
                        className={'login__box-login-btn'}
                        onClick={submit}
                      >Submit</button>
                      <p className={'login__box-login-signup'}>
                          Don't have an account yet?
                          <a href={'./Registration'}>Sign Up</a>
                      </p>
                  </div>
              </div>
          </div>
        )
    } else {
        return <ToDos id={id}/>
    }
}

export default Login;
