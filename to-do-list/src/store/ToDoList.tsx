import React, {useEffect} from 'react';
import { toJS, runInAction } from 'mobx';
import { observer, useLocalObservable } from 'mobx-react';
import List from '../components/List';
import Form from '../components/Form';
import '../styles/ToDos.scss'

interface Props {
    id: number,
}

export const ToDos: React.FunctionComponent<Props> = observer((props): JSX.Element => {
  useEffect(() => {
    const authorizationData = JSON.parse(localStorage.authorizationData)
    store.getTodos(authorizationData.userId)
  }, [])
  const store = useLocalObservable(() => {
    return {
      todos: [],
      getTodos : async (userId: number) => {
        try {
          await fetch('/api/todo/get', {
          method: 'POST',
            body: JSON.stringify({ userId }),
          headers: {
            'Content-type': 'application/json'
          }
        })
          .then(response => response.json())
          .then(data => {
            runInAction(() => {
              store.todos = data;
            })
          })
      } catch (e) {
          alert('Something is wrong... Try latter!')
        }},
      removeTodo: async (todoId: number, userId: number) => {
        try {
          await fetch('/api/todo', {
            method: 'DELETE',
            body: JSON.stringify({ todoId, userId }),
            headers: {
              'Content-type': 'application/json'
            }
          })
            .then(response => response.json())
            .then(data => {
              runInAction(() => {
                store.todos = data
              })
            })
        } catch (e) {
          alert('Something is wrong... Try latter!')
        }
      },
      addTodos: async (text: string, userId: number) => {
        try {
          await
            fetch('/api/todo', {
              method: 'POST',
              body: JSON.stringify({ text, userId }),
              headers: {
                'Content-type': 'application/json'
              }
            })
              .then(response => response.json())
              .then(data => {
                runInAction(() => {
                  store.todos = data;
                })
              })
        } catch (e) {
          alert('Something is wrong... Try latter!')
        }
      },
      changeOfStatusTodos: async (isConfirmed: number, todosId: number, userId: number) => {
        try {
          await
            fetch('/api/todo', {
              method: 'PUT',
              body: JSON.stringify({ isConfirmed, todosId, userId }),
              headers: {
                'Content-type': 'application/json'
              }
            })
              .then(response => response.json())
              .then(data => {
                runInAction(() => {
                  store.todos = data
                })
              })
        } catch (e) {
          alert('Something is wrong... Try latter!')
        }
      },
    }
  })
  return(
      <div className={'todos'}>
          <div className={'todos__app'}>
              <Form addNewBrick={store.addTodos} userId={props.id}/>
              <List
                  removeTodo={store.removeTodo}
                  changeOfStatusTodos={store.changeOfStatusTodos}
                  userid={props.id}
                  todos={toJS(store.todos)}
              />
          </div>
      </div>
  )
})
