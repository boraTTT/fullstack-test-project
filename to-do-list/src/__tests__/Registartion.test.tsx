import React from 'react';
import { render, screen } from "@testing-library/react";
import Registration from '../components/Registration';

test('Invalid Input Warning', () => {
  render(<Registration />);
  screen.getByPlaceholderText('Full name').textContent = 'Andrey'
  expect(screen.getByPlaceholderText('Full name').textContent).toBe('Andrey');
});

test('Is there a field on the page with the placeholder password', () => {
  render(<Registration />);
  expect(screen.getByPlaceholderText('Password')).toBeInTheDocument();
});

test('Is there a field on the page with the placeholder confirm password', () => {
  render(<Registration />);
  expect(screen.getByPlaceholderText('Confirm password')).toBeInTheDocument();
});

test('Is there button `Submit`', () => {
  render(<Registration />)
  expect(screen.getByText('Submit')).toBeInTheDocument()
})
