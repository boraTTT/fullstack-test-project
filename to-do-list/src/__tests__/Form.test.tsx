import React from 'react';
import { screen, render } from "@testing-library/react";
import Form from '../components/Form';

const props = {
  addNewBrick: jest.fn(),
  userid: 1,
}

test('Is there a field on the page for write todo', () => {
  render(<Form
    addNewBrick={props.addNewBrick}
    userId={props.userid}
  />);
  const inputField = screen.getByPlaceholderText('Write to me!');
  expect(inputField).toBeInTheDocument();
});

test('Is there button add todo', () => {
  render(<Form
    addNewBrick={props.addNewBrick}
    userId={props.userid}
  />);
  const button = screen.getByRole('button')
  const buttonName = screen.getByText('Add to-do')
  expect(buttonName).toBeInTheDocument()
  expect(button).toBeInTheDocument()
});
