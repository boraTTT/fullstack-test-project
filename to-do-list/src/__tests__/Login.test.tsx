import React from 'react';
import { render, screen } from "@testing-library/react";
import Login from '../components/Login';
import userEvent from "@testing-library/user-event";

test('Is there a field on the page with the placeholder Full name', () => {
  render(<Login />);
  screen.getByPlaceholderText('Full name').textContent = 'Andrey'
  const placeholderText = screen.getByPlaceholderText('Full name').textContent;
  expect(placeholderText).toBe('Andrey');
});

test('Is there a field on the page with the placeholder password', () => {
  render(<Login />);
  expect(screen.getByPlaceholderText('Password')).toBeInTheDocument();
});

test('Is there button `Submit`', () => {
  render(<Login />)
  userEvent.click(screen.getByText('Sign Up'))
  expect(screen.getByText('Submit')).toBeInTheDocument()
})
