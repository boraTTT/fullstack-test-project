import React from 'react';
import { screen, render } from "@testing-library/react";
import ListItem from '../components/ListItem';

const props = {
  data: {
    id: 1,
    text: 'Todo content',
    isconfirmed: 0,
  },
  changeOfStatusTodos: jest.fn(),
  removeTodo: jest.fn(),
  userid: 1,
}

test('Should get correctly props', () => {
  render(<ListItem
    data={props.data}
    changeOfStatusTodos={props.changeOfStatusTodos}
    removeTodo={props.removeTodo}
    userid={props.userid}/>);
  const todo = screen.getByText('Todo content');
  expect(todo).toBeInTheDocument();
});

test('Is there checkbox change status todo', () => {
  render(<ListItem
    data={props.data}
    changeOfStatusTodos={props.changeOfStatusTodos}
    removeTodo={props.removeTodo}
    userid={props.userid}/>);
  const checkbox = screen.getByRole('checkbox')
  expect(checkbox).toBeInTheDocument()
});

test('Is there button delete todo', () => {
  render(<ListItem
    data={props.data}
    changeOfStatusTodos={props.changeOfStatusTodos}
    removeTodo={props.removeTodo}
    userid={props.userid}/>);
  const button = screen.getByRole('button')
  expect(button).toBeInTheDocument()
});
