const express = require('express');

const userRoutes = require('../todo-list/routes/user.routes.tsx');
const todoRoutes = require('../todo-list/routes/todo.routes.tsx');

const PORT = 5000;

const app = express();
app.use(express.json());
app.use('/api', userRoutes, todoRoutes);

app.listen(PORT, () => {
  console.log(`Server is running port: ${PORT}`);
});
