const db = require('../db.tsx')

class UserController {
  async createUser(req, res) {
    const { username, password, isLogin } = req.body
    const users = (await db.query(`SELECT * FROM "user"`)).rows
    if (isLogin) {
      const currentUser = users.find(user => user.username === username && user.password === password)
      if (currentUser) {
        res.json(currentUser.id)
      }
    } else {
      const newUser = await db.query(`INSERT INTO "user" (username, password) values ($1, $2) RETURNING *`, [username, password])
      res.json(newUser.rows[0].id)
    }
  }
  async updateUser(req, res) {
    const { id, username, password } = req.body
    const user = await db.query(`UPDATE "user" set username = $1, password = $2 where id = $3 RETURNING *`, [username, password, id])
    res.json(user.rows[0])
  }
  async deleteUser(req, res) {
    const id = req.params.id
    const user = await db.query('DELETE FROM "user" where id = $1', [id])
    res.json(user.rows[0])
  }
}

module.exports = new UserController()
