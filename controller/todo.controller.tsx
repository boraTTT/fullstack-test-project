const db = require('../db.tsx')

class TodoController {
  async createTodo(req, res) {
    const { text, userId } = req.body
    await db.query(`INSERT INTO "todo" (text, isConfirmed, userId) values ($1, $2, $3) RETURNING *`, [text, 0, userId])
    const todo = await db.query(`SELECT * FROM "todo" where userid = $1`, [userId])
    res.json(todo.rows)
  }
  async getTodo(req, res) {
    const { userId } = req.body
    const todo = await db.query(`SELECT * FROM "todo" where userid = $1`, [userId])
    res.json(todo.rows)
  }
  async updateTodo(req, res) {
    const { isConfirmed, todosId, userId } = req.body
    await db.query(`UPDATE "todo" set isConfirmed = $1 where id = $2`, [isConfirmed, todosId])
    const todo = await db.query(`SELECT * FROM "todo" where userid = $1`, [userId])
    res.json(todo.rows)
  }
  async deleteTodo(req, res) {
    const { todoId, userId } = req.body
    await db.query('DELETE FROM "todo" where (id = $1) and (userId = $2)', [todoId, userId])
    const todo = await db.query(`SELECT * FROM "todo" where userid = $1`, [userId])
    res.json(todo.rows)
  }
}

module.exports = new TodoController()
